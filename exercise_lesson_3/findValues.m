function [V] = findValues(P, R, gamma)

Imat = eye(size(P,1));
V = ((Imat - gamma * P)^-1 )* R;
end
