function [value] = value_iteration(P, R, gamma)

%%initialization
value = zeros(1,length(R));

eps = .001;
error = 100;
counter = 0;
while(error > eps)
    counter = counter +1;
    for i=1:length(R)
        for a = 1:size(P,3)
            temp(a)= sum(squeeze(P(i,:,a)).* value);
        end
        value_new(i) = R(i)+ gamma * max(temp);
    end
    error =  sum((value-value_new).^2);
    value = value_new;
end

        