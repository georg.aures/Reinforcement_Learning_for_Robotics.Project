function policy = policy_iteration(P, R, gamma)

%% initialization
policy = ones(size(R,1),1);
flag = 300;

while(flag ~= 0)
    for i = 1:length(R)
        P_temp(i,:)= P(i,:,policy(i));
    end
    V = findValues(P_temp,R, gamma);
    for a = 1:size(P,3)
            temp(:,a)= sum(squeeze(P(:,:,a)).* repmat(V',[length(R) 1]),2);
    end
    [value,ind] = max(temp,[],2);
    policy = ind;
    flag = flag -1;
end
    
    
