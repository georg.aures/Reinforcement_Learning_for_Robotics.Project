function [res] = func_dState(a1,da1,a2,da2,T)

m1 = 1.0;
m2 = 2.0;
l1 = 0.5;
l2 = 0.9;
g = 9.8;

dda1 = -(-2*T + da1^2*l1^2*m2*sin(2*a1 - 2*a2) + 2*da2^2*l1*l2*m2*sin(a1 - a2) + 2*g*l1*m1*sin(a1) + g*l1*m2*sin(a1) + g*l1*m2*sin(a1 - 2*a2));
         
D = 2*l1^2*(m1 - m2*cos(a1 - a2)^2 + m2);
D = sign(D) * max(D, eps) ;
dda1 = dda1/D; % divide by detorminator\n",

% theta 2 dot dot\n",
dda2 = (-T*cos(a1 - a2) + da1^2*l1^2*m1*sin(a1 - a2) + da1^2*l1^2*m2*sin(a1 - a2) + da2^2*l1*l2*m2*sin(a1 - a2)*cos(a1 - a2) + g*l1*m1*sin(a1)*cos(a1 - a2) - g*l1*m1*sin(a2) + g*l1*m2*sin(a1)*cos(a1 - a2) - g*l1*m2*sin(a2));
D = l1*l2*(m1 - m2*cos(a1 - a2)^2 + m2);
D = sign(D) * max(D, eps) ;
dda2 = dda2 / D; % divide by detorminator\n",

res = [da1 ; dda1 ; da2 ; dda2];

end

