% please execute the following line of code to run lesson7/exercise1

run('pilco/scenarios/ex_1_cart_spring/cartSpring_learn.m')

% remark: we changed the system dynamics of the cart-pole, such that the mass-spring-damper system is simulated
% (the pole has no effect, and there is an invisible spring and invisible damper)
% given the goal x=0.6 the algortihm is able to find the solution very fast.
% the algorithm first explores the wrong direction but gains results for the right direction too.
% this is an remarkable ability of the model.

% please execute the following line of code to run lesson7/exercise2

run('pilco/scenarios/ex_2_cartPole/cartPole_learn.m')

% remark: we just copied and understood the given solution from pilco
