%% dynamics_cp.m
% *Summary:* Implements ths ODE for simulating the cart-pole dynamics.
%
%    function dz = dynamics_cp(t, z, f)
%
%
% *Input arguments:*
%
%		t     current time step (called from ODE solver)
%   z     state                                                    [4 x 1]
%   f     (optional): force f(t)
%
% *Output arguments:*
%
%   dz    if 3 input arguments:      state derivative wrt time
%         if only 2 input arguments: total mechanical energy
%
%
% Note: It is assumed that the state variables are of the following order:
%       x:        [m]     position of mass
%       dx:       [m/s]   velocity of mass
%
%
% A detailed derivation of the dynamics can be found in: slides
% Last modified: 2017

function dz = dynamics_cp(t,z,f)
%% Code

k = 1;  % [N/m]    spring constant
m = 1;  % [kg]     mass
%c = 0.2; % [Ns/m]   friction coeff.
%c = 2; % [Ns/m]   friction coeff.
c = 0; % [Ns/m]   friction coeff.


if nargin==3
  dz = zeros(4,1);
  dz(1) = z(2);
  dz(2) = -k/m * z(1) - c/m * z(2) + f(t);
else
  dz = m*z(2)^2/2 + k*z(1)^2/2;
end