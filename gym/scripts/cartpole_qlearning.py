# Inspired by https://medium.com/@tuzzer/cart-pole-balancing-with-q-learning-b54c6068d947

import gym
import numpy as np
import math
from collections import deque
import pickle

class QCartPoleSolver():
    def __init__(self, buckets=(1, 1, 6, 12,), n_episodes=1000000, n_win_ticks=195, min_alpha=0.1, min_epsilon=0.0, gamma=1.0, ada_divisor=25, max_env_steps=None, quiet=False, monitor=False):
        self.buckets = buckets # down-scaling feature space to discrete range
        self.n_episodes = n_episodes # training episodes
        self.n_win_ticks = n_win_ticks # average ticks over 100 episodes required for win
        self.min_alpha = min_alpha # learning rate
        self.min_epsilon = min_epsilon # exploration rate
        self.gamma = gamma # discount factor
        self.ada_divisor = ada_divisor # only for development purposes
        self.quiet = quiet

        self.env = gym.make('CartPole-v0')
        if max_env_steps is not None: self.env._max_episode_steps = max_env_steps
        if monitor: self.env = gym.wrappers.Monitor(self.env, 'cartpole-1', force=True) # record results for upload

        self.Q = np.zeros(self.buckets + (self.env.action_space.n,))

    def discretize(self, obs):
        upper_bounds = [self.env.observation_space.high[0], 0.5, self.env.observation_space.high[2], math.radians(180)]
        lower_bounds = [self.env.observation_space.low[0], -0.5, self.env.observation_space.low[2], -math.radians(180)]
        ratios = [(obs[i] + abs(lower_bounds[i])) / (upper_bounds[i] - lower_bounds[i]) for i in range(len(obs))]
        new_obs = [int(round((self.buckets[i] - 1) * ratios[i])) for i in range(len(obs))]
        new_obs = [min(self.buckets[i] - 1, max(0, new_obs[i])) for i in range(len(obs))]
        return tuple(new_obs)

    def choose_action(self, state, epsilon):
        return self.env.action_space.sample() if (np.random.random() <= epsilon) else np.argmax(self.Q[state])

    def update_q(self, state_old, action, reward, state_new, alpha):
        self.Q[state_old][action] += alpha * (reward + self.gamma * np.max(self.Q[state_new]) - self.Q[state_old][action])

    def get_epsilon(self, t):
        return max(self.min_epsilon, min(1, 1.0 - math.log10((t + 1) / self.ada_divisor)))

    def get_alpha(self, t):
        return max(self.min_alpha, min(1.0, 1.0 - math.log10((t + 1) / self.ada_divisor)))

    def run(self):
        scores = deque(maxlen=100)
        #f = open("object.pkl", "rb")
        #self.Q = pickle.load(f)
        #f.close()

        for e in range(self.n_episodes):
            current_state = self.discretize(self.env.reset())

            alpha = self.get_alpha(e)
            epsilon = self.get_epsilon(e)
            done = False
            i = 0
            in_state = 0

            while not done:
                if (e>1000000):
                    self.env.render()
                action = self.choose_action(current_state, epsilon)
                obs, reward, done, ins_state = self.env.step(action)
                new_state = self.discretize(obs)
                self.update_q(current_state, action, reward, new_state, alpha)
                current_state = new_state
                i += 1
                if ins_state:
                    in_state += 1

            scores.append(i)
            mean_score = np.mean(scores)
            if in_state >= 200 and e >= 100:
                print(in_state)
                print(e)
                print(mean_score)
                print(self.n_win_ticks)
                if not self.quiet: print('Ran {} episodes. Solved after {} trials ✔'.format(e, e - 100))
                print(self.Q)
                f = open("object.pkl", "wb")
                pickle.dump(self.Q, f)
                f.close()
                iterat = 0
                self.min_epsilon = 0
                while iterat < 10:
                    current_state = self.discretize(self.env.reset())
                    done = False
                    in_state = 0
                    while not done:
                        self.env.render()
                        action = self.choose_action(current_state, epsilon)
                        obs, reward, done, ins_state = self.env.step(action)
                        self.update_q(current_state, action, reward, new_state, alpha)
                        new_state = self.discretize(obs)
                        current_state = new_state
                        if ins_state:
                            in_state += 1
                    iterat += 1
                    print(in_state)
                return e - 100
            if e % 100 == 0 and not self.quiet:
                print('[Episode {}] - Mean survival time over last 100 episodes was {} ticks.'.format(e, mean_score))
                print('[In state {}]'.format(in_state))
                if (in_state<5 and self.min_epsilon < 0.5):
                    self.min_epsilon = 0.5
                elif self.min_epsilon > 0.4 and in_state < 15:
                    self.min_epsilon = 0.1
                else:
                    self.min_epsilon = 0.0
            print('"[Episode {}][In state {}][Learning rate {}]" +'.format(e, in_state, alpha))

        if not self.quiet: print('Did not solve after {} episodes 😞'.format(e))
        return e

if __name__ == "__main__":
    solver = QCartPoleSolver()
    solver.run()
    # gym.upload('tmp/cartpole-1', api_key='')
