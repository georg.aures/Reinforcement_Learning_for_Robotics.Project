c = 0.2;
g = 0.6;
rng(1414)

A = [0 1;-1 -c];
B = [0 ; 1];


ws = [0 ; 0 ; 0];
ws_old = [inf;inf;inf];
w_log = ws;
q = 0;
p = zeros(20,4);
p(:,1) = inf;
ps = inf;
ps_old = ps;
p_log = ps;
n = 1;
while norm(ws - ws_old) > 0.01 % not(ps_old - ps < 0.000000001) % n < 5 % norm(ws - ws_old) > 0.00001 
    n = n + 1;
    for i = 1 : 20
        w = ws + 0.1*[rand().*(rand()>0.5);rand().*(rand()>0.5);rand().*(rand()>0.5)];
        sim('mds_sys_linpol.slx');
        r_weight = [0; r.time(2:end).*(r.time(2:end)-r.time(1:end-1))];
        q = -sum(r.data(:,1).*r_weight);
        p(i+10,:) = [q(1) ; w];
    end
    p = sortrows(p);
    ws_old = ws;
    ps_old = ps;
    ps = p(1,1);
    ws = ws + (transpose(p(1:10,2:4)) - ws) * p(1:10,1) / sum(p(1:10,1));
    w_log(:,n) = ws;
    p_log(n) = ps;
end

p_log
w_log

w = ws;
sim('inverted_pendulum.slx');
plot(y)