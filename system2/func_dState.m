function [res] = func_dState(x,y,a,b,F)

M = 1;
m = 0.2;
l = 0.5;
g = 9.8;

%x
%y = dx/dt
%z = d^2x/dt^2
%a
%b = da/dt
%c = d^2a/dt^2

% from wikipedia
%%1%% (M + m) * z - m*l*c*cos(a) + m*l*b^2*sin(a) = F
%%2%% l * c - g * sin(a) = z * cos(a)
% to wolfram
%solve (M + m) * z - m*l*c*cos(a) + m*l*b^2*sin(a) = F and l * c - g * sin(a) = z * cos(a) for c and z
%gives
z = -(b^2 * (-l) * m * sin(a) + g * m * sin(a) * cos(a) + F)/(m * cos(a)^2 - m - M);
c = -(b^2 * (-l) * m * sin(a) * cos(a) + F * cos(a) + g * m * sin(a) + g * M * sin(a))/(l * (m * cos(a)^2 - m - M));

res = [y ; z ; b ; c];

end

