function w_random = rand_w(d)
%RAND_W Summary of this function goes here
%   Detailed explanation goes here
w_random = zeros(d,1);
for i = 1:d
    w_random(i) = rand().*( rand() > (0.6*(1-1/d)) );
end
end

