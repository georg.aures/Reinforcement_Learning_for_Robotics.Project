
goal = [0, 0, pi, 0];
rng(1414)
explore_N = 2;
ws = zeros(9,1);
ws = [0.0270; 0.0057; 0.0079; 0.0594; 0.0445; 0.0544; 0.0589; 0.0451; 0.0412];
ws = [0.0592;0.0199;0.0106;0.1093;0.1205;0.0990;0.1167;0.0798;0.0738];
%=========================
d = size(ws,1);
ws_old = ones(d,1)*inf;
w_log = ws;
q = 0;
p = zeros(20,d+1);
p(:,1) = inf;
ps = inf;
ps_old = ps;
p_log = ps;
n = 1;
while norm(ws - ws_old) > 0.01 % not(ps_old - ps < 0.000000001) % n < 5 % norm(ws - ws_old) > 0.00001 
    n = n + 1;
    for i = 1 : explore_N
        w = ws + (2/max(n/explore_N,1) +0.1)*rand_w(d);
        sim('ip_sys_linpol.slx');
        r_weight = [0; r.time(2:end).*(r.time(2:end)-r.time(1:end-1))];
        q = -sum(r.data(:,3).*r_weight);
        p(i+10,:) = [q(1) ; w];
    end
    p = sortrows(p);
    ws_old = ws;
    ps_old = ps;
    ps = p(1,1);
    ws = ws + (transpose(p(1:10,2:d+1)) - ws) * p(1:10,1) / sum(p(1:10,1));
    w_log(:,n) = ws;
    p_log(n) = ps;
    n
    ws
    ps
end

p_log
w_log

w = ws;
sim('ip_sys_linpol.slx');
plot(y)