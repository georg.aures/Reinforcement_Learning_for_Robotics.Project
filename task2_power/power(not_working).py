# -*- coding: utf-8 -*-
"""
Created on Sat Jan 27 12:31:55 2018

@author: Manos
"""

#%% Imports

import numpy as np
import matplotlib.pyplot  as  plt
plt.rcParams['figure.figsize'] = (3.0, 4.0) #default plot size

# set seed
np.random.seed(7)

#%% System params

### define some constants
# model to use (change this to change the model)
model = 'spring'
# simulation time
tsim = 10
# simulation step
dt = 0.01
# define model params (change them to see different behaviours)
# spring - mass
m = 1
k = 1
# c = 0.2
c = 0.2
# cartpole
mc = 0.2
M = 1
l = 0.5
# doubpe pendulum
m1 = 1
m2 = 2
l1 = 0.5
l2 = 0.9
# other
g = 9.81
eps = 1e-6 # small constant to avoid zero division
Max = 1e6 # big value to avoid overflow
# define force (or torque) function (change to observe different behaviours)
F = lambda t :  np . sin ( t ) 

#%% Define system models

def dynamics(q, F, model):
    # This function calculates q_dot given q and F, for all three models
    # Inputs:
    # > q: an array of the system state q = [u1,...,un]. For example, for the spring, q = [u1 = , u2 = x_dot]
    # > F: the force for our system at time t
    # > model: a string indicating the model we are using ('spring', 'cartpole', 'pendulum')
    # Outputs:
    # > q_dot: the state derivative
    
    # define q_dot
    q_dot = np.zeros(q.shape)
    # distinguish cases
    if model == 'spring':
        # retrieve variables
        x = q[0]
        dx = q[1]
        # use model equation
        ddx = -(k/m)*x - (c/m)*dx + F
        # calculate q_dot
        q_dot[0] = dx
        q_dot[1] = ddx
        # done
    elif model == 'cartpole':
        # retrieve variables
        x = q[0]
        dx = q[1]
        a = q[2] # the angle theta
        da = q[3]
        # use model equations
        # theta dot dot
        # simple way to avoid tan overflow
        mytan = np.sin(a)
        mycos = np.cos(a)
        mycos = np.sign(mycos) * np.maximum(mycos, eps) 
        mytan /= mycos
        # avoid overflow by da (I have no idea why is happens!)
        if abs(da) > Max:
            da = np.sign(da) * Max
        dda = (F + (mc + M)*g*mytan + mc*l*np.cos(a) - da*da * mc*l*np.sin(a)) * np.cos(a)
        D = (mc+M)*l # detorminator 
        dda /= D # divide by detorminator
        # the same for x dot dot
        D = np.cos(a)
        # if abs(D) < eps, replace if with D = sign(D) * eps. This avoids division by very small numbers
        D = np.sign(D) * np.maximum(D, eps) 
        mytan = np.sin(a)
        mycos = np.cos(a)
        mycos = np.sign(mycos) * np.maximum(mycos, eps) 
        mytan /= mycos
        ddx = l*dda / D - g*mytan
        # calculate q_dot
        q_dot[0] = dx
        q_dot[1] = ddx
        q_dot[2] = da
        q_dot[3] = dda
        # done
    elif model == 'pendulum':
        # retrieve variables
        a1 = q[0]
        da1 = q[1]
        a2 = q[2]
        da2 = q[3]
        # use model equations
        # theta 1 dot dot
        T = F
        dda1 = -(-2*T + da1**2*l1**2*m2*np.sin(2*a1 - 2*a2) + 2*da2**2*l1*l2*m2*np.sin(a1 - a2) 
                 + 2*g*l1*m1*np.sin(a1) + g*l1*m2*np.sin(a1) + g*l1*m2*np.sin(a1 - 2*a2))
        D = 2*l1**2*(m1 - m2*np.cos(a1 - a2)**2 + m2)
        D = np.sign(D) * np.maximum(D, eps) 
        dda1 /= D # divide by detorminator
        # theta 2 dot dot
        dda2 = (-T*np.cos(a1 - a2) + da1**2*l1**2*m1*np.sin(a1 - a2) + da1**2*l1**2*m2*np.sin(a1 - a2) 
                + da2**2*l1*l2*m2*np.sin(a1 - a2)*np.cos(a1 - a2) + g*l1*m1*np.sin(a1)*np.cos(a1 - a2) 
                - g*l1*m1*np.sin(a2) + g*l1*m2*np.sin(a1)*np.cos(a1 - a2) - g*l1*m2*np.sin(a2))
        D = l1*l2*(m1 - m2*np.cos(a1 - a2)**2 + m2)
        D = np.sign(D) * np.maximum(D, eps) 
        dda2 /= D # divide by detorminator
        # calculate q_dot
        q_dot[0] = da1
        q_dot[1] = dda1
        q_dot[2] = da2
        q_dot[3] = dda2
        # done
    else:
        # wrong input
        q_dot = None
    # return
    return q_dot

def nextState(q, qdot, dt = 1e-4):
    # computes the next state of the system
    # Input:
    # > q: current state
    # > qdot: state derivative
    # > dt: time step
    # Output:
    # qnext: state at time t + dt
    return q + qdot * dt

def reward(q, q_req, tol = 1e-4):
    # the reward function of our problems
    # Input:
    # > q: the current system state
    # > q_req: the required system state
    # > tol: tolerance - if ||q - q_req|| < tol, we consider
    #   that q_req has been reached
    # Output:
    # > rew: the reward of state q
    
    # we choose a simple inverse quadratic norm reward
    # the further from the required state we are, 
    # the smallest rew is
    dist =  np.linalg.norm(q - q_req)
    # if q_req reached, reward is zero
    if dist < tol:
        rew = 0
    # else assign the negative of the Euclidean norm
    else:
        rew = - np.linalg.norm(q - q_req)
    return rew

def mu(q, w, typ = 'linear'):
    # a policy function
    # Input:
    # > q: current state
    # > w: policy params
    # > typ: policy type ('linear', etc.)
    # Output:
    # > u: the action
    if typ == 'linear':
        u = w.dot(q)
    else:
        u = 0
    return u

def sim(q0, mu, w, tsim, dt, model):
    # simulates the system for tsim seconds, and stores the results
    # It calculates q_dot from dynamics function, and calcs the next system state as q_next = q_prev + dt*q_dot
    # Inputs:
    # > q: the initial state of the system
    # > mu: the force function for our system
    # > w: params of policy
    # > tsim: the time to simulate (tsim seconds)
    # > dt: simulation timestep
    # > model: a string indicating the model we are using ('spring', 'cartpole', 'pendulum')
    # Outputs:
    # > values: an array of size len(q) * timesteps of the simulated values. The first row is the times, the 2nd row
    #           the values of u1 at all timesteps, the 3rd row is u2, etc., for all state variables of the system
    
    # init values array
    N = int(tsim / dt) + 1 # total number of simulation steps
    s = len(q0)
    values = np.zeros((s+1, N))
    for i in range(s):
        values[i+1,0] = q0[i]
    
    # start simulation
    tk = dt
    i = 0
    q = q0.copy()
    while tk <= tsim:
        # caluclate next state
        F = mu(q, w)
        q += dt * dynamics(q, F, model)
        # save it in values
        i += 1
        values[0,i] = tk
        for j in range(s):
            values[j+1,i] = q[j]
        # increase timestep
        tk += dt
    # return 
    return values
    

#%% Model free approach 1: POWER

def power(q0, q_goal, model, s, a = 0.99, maxIter = 1000000, dt = 1e-3):
    # power algorithm for a linear policy
    # Inputs:
    # > q0: inittal state
    # > q_goal: goal state
    # > model: a string for the system model ('spring', 'cartpole;, etc)
    # > s: number of rollouts
    # > a: the discount factor
    # > maxIter: maximum number of iterations
    # Outputs:
    # > w: linear policy params
    
    # initialize
    T = 1000 # rollouts duration (steps)
    eps = 1e-6
    # random initial policy
    w = np.zeros(len(q0))
    wnew = np.ones(len(q0))
    n = 0 # counter 
    while n < maxIter:
        n += 1
        # run s rollouts
        R = np.zeros(2*s) # rewards of rollouts
        W = np.zeros([len(q0), 2*s]) # weights of rollouts
        d = np.linalg.norm(w - wnew)
        if d < eps:
            break
        w = wnew.copy()
        for i in range(2*s):
            # perturb policy params randomly
            w_temp = w + np.random.randn(len(q0))
            W[:,i] = w_temp
            # run rollout with these params and collect reward
            q = q0 # initial state
            for j in range(T):
                # take action
                u = w_temp.dot(q)
                # compute next state
                qdot = dynamics(q, u, model)
                q = nextState(q, qdot)
                # get reward
                R[i] += a**j * reward(q, q_goal)
        # rollouts done
        # collect s best rollouts and their weights
        idx = np.argsort(R)[0:s]
        W = W[:,idx].copy()
        R = R[idx].copy()
        # update policy params
        wnew = np.sum(R * (W - w[:,None]), axis = 1) / np.sum(R)
    # end while
    return w
        
#%% MAIN PROGRAM - simulate a system with a given policy
    
# model to use (change this to change the model)
model = 'spring'
# simulation time
tsim = 50
# define force (or torque) function (change to observe different behaviours)
# F = lambda t :  np . sin ( t ) 
# F = lambda t :  1 # step function
# init. state
if model == 'spring':
    q0 = np.zeros((2,1))
else:
    q0 = np.zeros((4,1))
# change q0 here if you like (for example, set q0[1] = 3, etc.)

# run the model
q0 = np.ones((2,1))
q_goal = np.array([0.6, 0])
w = power(q0, q_goal, model, 10)
#w = np.array([0,-1])
simValues = sim(q0, mu, w, tsim, dt, model)

# plot the results
if model == 'spring':
    t = simValues[0,:]
    x = simValues[1,:]
    dx = simValues[2,:]
    plt.plot(t,x,'b', label='x(t)')
    plt.plot(t,dx,'r', label='v(t)')
    plt.title('System simulation')
    plt.xlabel('t(s)')
    plt.ylabel('q(t)')
    plt.legend(loc='upper right')
    plt.show()
elif model == 'cartpole':
    t = simValues[0,:]
    x = simValues[1,:]
    dx = simValues[2,:]
    a = simValues[3,:]
    da = simValues[4,:]
    plt.plot(t,x,'b', label='x(t)')
    plt.plot(t,dx,'r', label='v(t)')
    plt.plot(t,a,'g', label='theta(t)')
    plt.plot(t,da,'y', label='omega(t)')
    plt.title('System simulation')
    plt.xlabel('t(s)')
    plt.ylabel('q(t)')
    plt.legend(loc='upper right')
    plt.show()
elif model == 'pendulum':
    t = simValues[0,:]
    a1 = simValues[1,:]
    da1 = simValues[2,:]
    a2 = simValues[3,:]
    da2 = simValues[4,:]
    plt.plot(t,a1,'b', label='theta1(t)')
    plt.plot(t,da1,'r', label='omega1(t)')
    plt.plot(t,a2,'g', label='theta2(t)')
    plt.plot(t,da2,'y', label='omega2(t)')
    plt.title('System simulation')
    plt.xlabel('t(s)')
    plt.ylabel('q(t)')
    plt.legend(loc='upper right')
    plt.show()
else:
    print("Wrong input!")
# done!
    


